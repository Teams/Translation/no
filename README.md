# Den norske oversettelsesgruppen til GNOME 🇳🇴

Velkommen! Vi oversetter GNOME til bokmål og nynorsk. Målet vårt er at alle som kan norsk, skal foretrekke å bruke GNOME på norsk. For å oppnå dette, etterstreber vi disse tingene:

- **Kvalitet:** Oversettelsene må være lettleste og korrekte.
- **Dekning:** Vi må dekke hele plattformen, fra kjerne- til tredjepartsprogramvare.
- **Tempo:** GNOME-økosystemet oppdateres kontinuerlig, så vi må være tidlig ute med nye og oppdaterte oversettelser.

## Bli med!

Du er velkommen som oversetter! Det er en flott måte å bidra til fellesskapet på. Ta kontakt med koordinator [Brage](https://bragefuglseth.dev) eller åpne en sak i arbeidsområdet vårt her på GitLab for å komme i gang. Vi henger i [GNOME Scandinavia](https://matrix.to/#/#scandinavia:gnome.org)-samtalen på Matrix, kom gjerne og slå av en prat med oss der! Vi snakkes 😁

## Status

De norske oversettelsene av GNOME vil i løpet av 2024/2025 gå gjennom en stor oppfriskning. Språkbruken vil over tid bli oppdatert og samordnet, og uoversatte kjerneområder vil endelig bli fylt ut. Dette er tidkrevende, og gjøres på dugnad, så heng med mens det jobbes! Det kommer til å være verdt det til slutt.

## Retningslinjer

- Bruk norske ord.
- Jobb hardt for å skrive lettlest.
- Sjekk kontekst hvis du er usikker.
- Bruk sjevroner som anførselstegn, ikke de vanlige engelske! « og » kan skrives med henholdsvis <key>AltGr</key> + <key>Z</key> og <key>AltGr</key> + <key>X</key> på Linux-systemer. Den norske typografen Torbjørn Eng har skrevet en [interessant artikkel](http://www.typografi.org/sitat/sitatart.html) om akkurat dette.
- Overskrifter har bare stor bokstav i første ord og egennavn, og bruker ikke engelsk Title Case.
- Tenk at du skal oversette *budskap*, ikke nødvendigvis de eksakte ordene som brukes til å formidle dem.
- Bokmålsoversettelsene gjøres på moderat bokmål. Bruk [Det norske akademis ordbok](https://naob.no) som rettesnor.
- Nynorskoversettelsene bruker [Nynorskordboka](https://ordbokene.no/nob/nn) som rettesnor.

## Unntak

- Vi oversetter ikke navn på tredjepartsapper, selv ikke hvis navnene er generiske. «Audio Sharing» vil for eksempel bli hetende det samme når appen oversettes til norsk. Grunnen til dette, er at vi ser på det som et egennavn som hovedsaklig brukes til å henvise til den spesifikke appen, og dermed vil bli mindre nyttig om det heter noe annet for norske brukere. GNOME sine «kjerneapper», som Filer og Innstillinger, oversetter vi derimot navnene til.
- Vi bruker «app». Ordet «app» er et fullverdig lånord som har fått innpass i Bokmålsordboka og Nynorskordboka. Moderne GNOME-apper krysser enhetsgrenser mellom datamaskiner, nettbrett og mobiler, og GNOME-fellesskapet har gått over til å bruke det der det tidligere ble brukt «application» på engelsk. Det er et kortere og mer elegant ord enn «program» eller «applikasjon».

## Vanlige mønstre

- Når noe har gått galt: «Dette mislyktes». Eksempel: «Failed to Connect» - «Tilkobling mislyktes»
- Når noe ikke blir funnet: «Fant ikke dette» eller «Fant ingen ting». Eksempel: «Device not Found» - «Fant ikke enhet» eller «Fant ingen enheter», avhengig av situasjon. Unntak: «No Results» oversettes til «Ingen treff».
- Når enheten ikke har nettilgang: «Ikke på nett».
- Når noe lastes inn: «Laster inn (evt. ord)». Eksempel: «Loading Updates» - «Laster inn oppdateringer».
- «Parent» og «child» eller «sub»: «over-» og «under». Eksempler: «Delete subfolder» - «Slett undermappe». «Move parent element» - «Flytt overelement».
- Prosent: Alltid mellomrom før prosenttegn. Eksempel: 98 %.

## Interessant lesestoff

- [Suffiksene «-sjon» og «-ing»](https://www.sprakradet.no/svardatabase/sporsmal-og-svar/suffiksene--sjon-og--ing/) - Språkrådet

## Ordbank

Målet med denne er ikke å skrive en norsk-engelsk ordbok, men heller å ha en enkel liste over GNOME/data/Linux-spesifikk terminologi som brukes på tvers av prosjekter, men ikke nødvendigvis er helt selvsagt. Enkelte ord er ment som forenklinger til bruk i brukergrensesnitt, og samsvarer kanskje ikke med de nøyaktige tekniske begrepene.

| Engelsk                       | Norsk                                        |
| ----------------------------- | -------------------------------------------- |
| sandboxing, sandboxed         | innhegning, innhegnet                        |
| daemon                        | tjeneste                                     |
| window manager, compositor    | visningssystem                               |
| cache                         | hurtiglagring                                |
| drag-and-drop, drop target    | dra-og-slipp, slippunkt                      |
| zoom level, zoom in, zoom out | skalering, forstørr, forminsk                |
| verbose logging               | full logging                                 |
| credentials                   | innloggingsinformasjon
